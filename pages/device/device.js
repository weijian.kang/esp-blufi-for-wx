var blufi = require("../../utils/blufi.js");
var util = require("../../utils/util.js");
var _this = this

Page({
  data: {
    deviceId: '',
    connected: true,
    ssid: '',
    password: '',
  },

  onShow: function (options) {
    this.initWifi()
  },

  onLoad: function (options) {
    _this = this
    this.setData({
      name: options.name,
      deviceId: options.deviceId,
    })

    blufi.listenDeviceMsgEvent(this.deviceMsgEventCallBack);
    blufi.initBleEsp32(options.deviceId)
    util.showLoading("设备初始化中")
  },

  onUnload: function () {
    blufi.disconnectBle(this.data.deviceId, this.data.name);
    blufi.unListenDeviceMsgEvent();
  },

  deviceMsgEventCallBack: function (options) {
    switch (options.type) {
      case blufi.BLUFI_TYPE.TYPE_STATUS_CONNECTED:
        _this.setData({
          connected: options.result
        });
        if (!options.result) {
          wx.showModal({
            title: '很抱歉提醒你！',
            content: '小程序与设备异常断开',
            showCancel: false, 
            success: function (res) {
              wx.navigateBack({
                delta: 1,
              })
            },
          })
        }
        break;
      case blufi.BLUFI_TYPE.TYPE_CONNECT_ROUTER_RESULT:
        wx.hideLoading();
        if (!options.result)
          wx.showModal({
            title: '温馨提示',
            content: '配网失败，请重试',
            showCancel: false,
          })
        else {
          if (options.data.progress == 100) {
            wx.showModal({
              title: '温馨提示',
              content: `连接成功路由器【${options.data.ssid}】`,
              showCancel: false, //是否显示取消按钮
              success: function (res) {
                wx.setStorage({
                  key: options.data.ssid,
                  data: _this.data.password
                })
              }
            })
          }
        }
        break;
      case blufi.BLUFI_TYPE.TYPE_RECIEVE_CUSTON_DATA:
        console.log("收到设备发来的自定义数据结果：", (options.data))
        wx.showModal({
          title: '收到自定义设备数据',
          content: `【${options.data}】`,
          showCancel: false, //是否显示取消按钮
        })
        break;
      case blufi.BLUFI_TYPE.TYPE_INIT_ESP32_RESULT:
        wx.hideLoading();
        console.log("初始化结果：", JSON.stringify(options))
        if (!options.result) {
          wx.showModal({
            title: '温馨提示',
            content: `设备初始化失败`,
            showCancel: false, //是否显示取消按钮
            success: function (res) {
              wx.navigateBack({
                delta: 1,
              })
            }
          })
        }
        break;
    }
  },

  okBtnClick: function () {
    if (!this.data.ssid) {
      util.showToast("SSID不能为空")
      return
    }
    if (!this.data.password) {
      util.showToast("密码不能为空")
      return;
    }
    util.showLoading("正在配网")
    console.log(this.data)
    blufi.sendSsidAndPassword(this.data.ssid, this.data.password)
  },

  onSSIDInput: function (e) {
    this.setData({
      ssid: e.detail.value
    })
  },

  onPasswordInput: function (e) {
    this.setData({
      password: e.detail.value
    })
  },

  initWifi() {
    wx.startWifi();
    wx.getConnectedWifi({
      success: function (res) {
        if (res.wifi.SSID.indexOf("5G") != -1) {
          wx.showToast({
            title: '当前为5G网络',
            icon: 'none',
            duration: 3000
          })
        }
        let password = wx.getStorageSync(res.wifi.SSID)
        console.log("password=>", password)
        _this.setData({
          ssid: res.wifi.SSID,
          password: password == undefined ? "" : password
        })
      },
      fail: function (res) {
        console.log(res);
        _this.setData({
          ssid: null,
        })
      }
    });
  }
})