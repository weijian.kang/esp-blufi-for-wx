const app = getApp()
var blufi = require("../../utils/blufi.js");
let _this = null;

Page({
    data: {
        devicesList: [],
        searching: false,
    },

    onLoad: function () {
        _this = this;
        blufi.init();
        blufi.listenDeviceMsgEvent(this.deviceMsgEventCallBack);
    },
    
    onUnload: function () {
        blufi.unListenDeviceMsgEvent();
    },

    deviceMsgEventCallBack: function (options) {
        switch (options.type) {
            case blufi.BLUFI_TYPE.TYPE_GET_DEVICE_LISTS:
                if (options.result)
                    _this.setData({
                        devicesList: options.data
                    });
                break;
            case blufi.BLUFI_TYPE.TYPE_CONNECTED:
                console.log("连接回调：" + JSON.stringify(options))
                wx.hideLoading()
                if (options.result) {
                    wx.showToast({
                        title: '连接成功'
                    })
                    wx.navigateTo({
                        url: '../device/device?deviceId=' + options.data.deviceId + '&name=' + options.data.name,
                    });
                } else {
                  wx.showToast({
                      title: '连接失败'
                  })
                }
                break;
            case blufi.BLUFI_TYPE.TYPE_GET_DEVICE_LISTS_START:
                if (!options.result) {
                    console.log("蓝牙未开启 fail =》", options)
                    wx.showToast({
                        title: '蓝牙未开启',
                        icon: 'none'
                    })
                } else {
                    //蓝牙搜索开始
                    _this.setData({
                        searching: true
                    });
                }
                break;
            case blufi.BLUFI_TYPE.TYPE_GET_DEVICE_LISTS_STOP:
                console.log('蓝牙停止搜索'+options.result?"ok":"ng")
                _this.setData({
                    searching: false
                });
                break;
        }
    },

    searchBtnClick: function () {
        if (this.data.searching) {
            blufi.stopDiscoverBle()
        } else {
            blufi.startDiscoverBle()
        }
    },

    connectBtnClick: function (e) {
        blufi.stopDiscoverBle()
        for (var i = 0; i < _this.data.devicesList.length; i++) {
            if (e.currentTarget.id === _this.data.devicesList[i].deviceId) {
                let name = _this.data.devicesList[i].name
                console.log('点击了，蓝牙准备连接的deviceId:' + e.currentTarget.id)
                blufi.connectBle(e.currentTarget.id, name);
                wx.showLoading({
                    title: '连接蓝牙设备中...',
                })
            }
        }
    },
});