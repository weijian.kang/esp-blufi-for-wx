import Message from 'tdesign-miniprogram/message/index';
var onfire = require("../../utils/onfire.js");

Page({
    data: {},
    onEvent1Btn(e) {
        console.log("订阅事件1")
        onfire.on(
            "event1",
            function (data) {
                console.log("事件1回调，数据=" + data)
                Message.info({
                    offset: [20, 32],
                    duration: 2000,
                    icon: false,
                    content: "事件1回调，数据=" + data,
                });
            }
        )
    },
    unEvent1Btn(e) {
        console.log("取消订阅事件1")
        onfire.un("event1")
    },
    oneEvent2Btn(e) {
        console.log("单次订阅事件2")
        onfire.one(
            "event2",
            function (data) {
                console.log("事件2回调，数据=" + data)
            }
        )
    },
    unEvent2Btn(e) {
        console.log("取消订阅事件2")
        onfire.un("event2")
    },
    clearBtn(e) {
        console.log("取消所有订阅")
        onfire.clear()
    },
    fireEvent1Btn(e) {
        console.log("异步触发事件1(数据123)")
        onfire.fire("event1", 123)
    },
    fireSyncEvent1Btn(e) {
        console.log("同步触发事件1(数据456)")
        onfire.fire("event1", 456)
    },
    fireEvent2Btn(e) {
        console.log("异步触发事件2(数据789)")
        onfire.fire("event2", 789)
    },
    fireSyncEvent2Btn(e) {
        console.log("同步触发事件2(数据012)")
        onfire.fireSync("event2", "012")
    },
});