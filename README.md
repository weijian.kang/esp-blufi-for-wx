# EspBlufiForWx

#### 一、介绍
基于微信小程序和Esp Blufi实现 WiFi配网

#### 二、软件框架
- blufi功能很大程度借鉴了 [BlufiEsp32WeChat](https://github.com/xuhongv/BlufiEsp32WeChat) 这个开源项目
- UI部分基于TDesign - 组件库模板
- 工具类用到了onfire库，有兴趣的同学可以去了解下：[微信小程序开发笔记 进阶篇③——onfire.js事件订阅和发布在微信小程序中的使用](https://blog.csdn.net/kangweijian/article/details/126206654)
- 软件框架如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/0c4cdef21bab4be085d370f59e638778.png)

```mermaid
graph TB
设备搜索界面 --> 搜索设备
设备搜索界面 --> 连接设备
配网界面 --> 初始化设备
配网界面 --> 发送WiFi的ssid和密码
```

#### 三、软件流程

   <img src="https://img-blog.csdnimg.cn/45d91bae4087450da8fa9ca322a63f27.jpeg" width="360px" /> 
   <img src="https://img-blog.csdnimg.cn/ae7606fd93404ef386cf8a43be760965.jpeg" width="360px" />  


- 设备搜索界面包含```设备列表```和```搜索设备按钮```
	- 点击```搜索设备按钮```，则```blufi.startDiscoverBle()```
	- 数据通过```deviceMsgEventCallBack```回调回来，显示到```设备列表```
	- 点击设备列表某行，则```blufi.connectBle(deviceId, name)```
	- 连接成功消息通过```deviceMsgEventCallBack```回调回来，界面跳转到配网界面
- 配网界面包含```ssid输入框```、```密码输入框```和```配网按钮```
	- ```onLoad```进来后就```blufi.initBleEsp32(deviceId)```
	- 点击```配网按钮```，则```blufi.sendRouterSsidAndPassword(...)```
	- 配网成功消息通过```deviceMsgEventCallBack```回调回来

#### 四、API介绍
   - blufi功能的实现主要基于```blufi.js```
   - 这里主要介绍```blufi.js```的API使用

| 函数                                      | 说明                 |
| ----------------------------------------- | -------------------- |
| ```init()```                              | 初始化               |
| ```listenDeviceMsgEvent(funtion)```       | 监听设备消息事件     |
| ```unListenDeviceMsgEvent()```            | 取消监听设备消息事件 |
| ```startDiscoverBle()```                  | 开始搜索蓝牙设备     |
| ```stopDiscoverBle()```                   | 停止搜索蓝牙设备     |
| ```connectBle()```                        | 连接蓝牙设备         |
| ```disconnectBle()```                     | 断开连接蓝牙设备     |
| ```initBleEsp32()```                      | 初始化Esp32蓝牙设备  |
| ```initBleEsp32()```                      | 初始化Esp32蓝牙设备  |
| ```sendSsidAndPassword(ssid, password)``` | 发送WiFi ssid和密码  |
| ```sendCustomData(customData)```          | 发送自定义数据       |

#### 五、全部源码
源码地址：[小康师兄 / EspBlufiForWx](https://gitee.com/weijian.kang/esp-blufi-for-wx)
 
博客介绍：[ESP32-C3入门教程 蓝牙篇③——基于微信小程序和Esp Blufi实现 WiFi配网](https://blog.csdn.net/kangweijian/article/details/126285240)

全部源码已经开源，欢迎star！